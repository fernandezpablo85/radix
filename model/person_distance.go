package model

type PersonDistance struct {
	Score     float64 `json:"score"`
	Candidate Person  `json:"candidate"`
}
