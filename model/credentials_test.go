package model

import "testing"

func TestCredentialsOk(t *testing.T) {
	var tests = []struct {
		credentials   Credentials
		expectedError error
	}{
		{Credentials{"email@foo.com", "pass"}, nil},
		{Credentials{"", "pass"}, EmptyEmail},
		{Credentials{"email@foo.com", ""}, EmptyPassword},
	}

	for _, ts := range tests {
		err := ts.credentials.Ok()
		if err != ts.expectedError {
			t.Fatalf("expected error %v but got %v ", ts.expectedError, err)
		}
	}
}
