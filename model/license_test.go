package model

import "testing"

func TestLicenseOk(t *testing.T) {
	var tests = []struct {
		licese        License
		expectedError error
	}{
		{License{"type-1", "1232123"}, nil},
		{License{"", "1232123"}, EmptyLicenseType},
		{License{"type-1", ""}, EmptyLicenseNumber},
	}

	for _, ts := range tests {
		err := ts.licese.Ok()
		if err != ts.expectedError {
			t.Fatalf("expected error %v but got %v ", ts.expectedError, err)
		}
	}
}
