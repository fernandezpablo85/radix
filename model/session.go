package model

type Session struct {
	Id             int     `json:"id"`
	Email          string  `json:"email"`
	Hash           string  `json:"-"`
	PersonId       int     `json:"personId"`
	Profile        Profile `json:"profile"`
	OrganizationId int     `json:"organizationId"`
	Created        int     `json:"created,omitempty"`
}

type SessionWithPerson struct {
	Session
	Person Person
}

func (swp SessionWithPerson) Ok() error {
	return swp.Person.Ok()
}

func (s Session) AddPerson(p Person) SessionWithPerson {
	return SessionWithPerson{s, p}
}
