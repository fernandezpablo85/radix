package model

import "errors"

type Profile struct {
	Professions []string  `json:"professions"`
	Specialties []string  `json:"specialties"`
	Licenses    []License `json:"licenses,omitempty"`
}

var EmptyProfessions = errors.New("Empty profile professions")

func (p Profile) Ok() error {
	if len(p.Professions) <= 0 {
		return EmptyProfessions
	}

	if len(p.Licenses) > 0 {
		for _, license := range p.Licenses {
			if err := license.Ok(); err != nil {
				return err
			}
		}
	}

	return nil
}
