package model

import "testing"

func TestProfileOk(t *testing.T) {
	var tests = []struct {
		profile       Profile
		expectedError error
	}{
		{Profile{[]string{"profession-1"}, []string{"specialty-1"}, []License{License{"lic-type", "123123"}}}, nil},
		{Profile{[]string{"profession-1"}, []string{"specialty-1"}, []License{License{"lic-type", "123123"}}}, nil},
		{Profile{[]string{"profession-1"}, []string{}, []License{}}, nil},
		{Profile{[]string{}, []string{"specialty-1"}, []License{}}, EmptyProfessions},
		{Profile{[]string{"profession-1"}, []string{}, []License{License{"lic-type", ""}}}, EmptyLicenseNumber},
		{Profile{[]string{"profession-1"}, []string{}, []License{License{"", "123123"}}}, EmptyLicenseType},
	}

	for _, ts := range tests {
		err := ts.profile.Ok()
		if err != ts.expectedError {
			t.Fatalf("expected error %v but got %v ", ts.expectedError, err)
		}
	}
}
