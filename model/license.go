package model

import "errors"

type License struct {
	Type   string `json:"type"`
	Number string `json:"number"`
}

var (
	EmptyLicenseType   = errors.New("Empty license type")
	EmptyLicenseNumber = errors.New("Empty license number")
)

func (l License) Ok() error {
	if len(l.Type) <= 0 {
		return EmptyLicenseType
	}
	if len(l.Number) <= 0 {
		return EmptyLicenseNumber
	}
	return nil
}
