package model

import (
	"testing"
	"time"
)

var date, _ = time.Parse(time.RFC3339, "1985-01-17T00:00:00Z")
var bdate = date.Unix()

var tests = []struct {
	candidate   Person
	err         error
	explanation string
}{
	// valid
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "Fernandez", "Ferrucci", "M", bdate, time.Now().Unix()}, nil, "OK"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "", "Ferrucci", "M", bdate, time.Now().Unix()}, nil, "empty last name"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "Fernandez", "", "M", bdate, time.Now().Unix()}, nil, "empty moms last name"},

	// not valid
	{Person{1, "DSI", "31282228", "Pablo", "Joe", "Fernandez", "Ferrucci", "M", bdate, time.Now().Unix()}, InvalidPersonId, "bad id type"},
	{Person{1, "", "31282228", "Pablo", "Joe", "Fernandez", "Ferrucci", "M", bdate, time.Now().Unix()}, InvalidPersonId, "empty id type"},
	{Person{1, "DNI", "123", "Pablo", "Joe", "Fernandez", "Ferrucci", "M", bdate, time.Now().Unix()}, InvalidPersonId, "short id"},
	{Person{1, "DNI", "", "Pablo", "Joe", "Fernandez", "Ferrucci", "M", bdate, time.Now().Unix()}, InvalidPersonId, "empty id"},
	{Person{1, "DNI", "31282228", "", "Joe", "Fernandez", "Ferrucci", "M", bdate, time.Now().Unix()}, InvalidPersonName, "empty first name"},
	{Person{1, "DNI", "31282228", "P", "Joe", "Fernandez", "Ferrucci", "M", bdate, time.Now().Unix()}, InvalidPersonName, "short first name"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "", "", "M", bdate, time.Now().Unix()}, InvalidPersonName, "empty both lasts names"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "F", "Ferrucci", "M", bdate, time.Now().Unix()}, InvalidPersonName, "last name present but short"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "Fernandez", "F", "M", bdate, time.Now().Unix()}, InvalidPersonName, "moms last name present but short"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "Fernandez", "Ferrucci", "", bdate, time.Now().Unix()}, InvalidPersonGender, "empty gender"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "Fernandez", "Ferrucci", "X", bdate, time.Now().Unix()}, InvalidPersonGender, "invalid gender"},
	{Person{0, "", "", "root", "root", "root", "root", "M", bdate, 0}, nil, "root is always valid"},
	{Person{1, "DNI", "31282228", "Pablo", "Joe", "Fernandez", "Ferrucci", "M", bdate, 0}, InvalidPersonCreationTime, "people with id > 0 can't have creation time zero"},

	// date validation?
	// {Person{0, "DNI", "31282228", "Pablo", "Fernandez", "Ferrucci", "M", nil}, false, "empty date"},
	// {Person{0, "DNI", "31282228", "Pablo", "Fernandez", "Ferrucci", "M", "1985-01-17"}, false, "bad format date"},
}

func TestPersonCreation(tx *testing.T) {
	for _, t := range tests {
		err := t.candidate.Ok()
		if err != t.err {
			tx.Fatalf("expected error %v but got %v (%s)", t.err, err, t.explanation)
		}
	}
}
