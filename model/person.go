package model

import (
	"errors"
	"strings"
)

type Person struct {
	Sid       int    `json:"sid"`
	IdType    string `json:"idType"`
	Id        string `json:"id"`
	FirstName string `json:"firstName"`
	OtherName string `json:"otherName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
	Moms      string `json:"mothersLastName,omitempty"`
	Gender    string `json:"gender"`
	BirthDate int64  `json:"birthDate"`
	Created   int64  `json:"created"`
}

var (
	InvalidPersonId           = errors.New("invalid person id")
	InvalidPersonGender       = errors.New("invalid person gender")
	InvalidPersonName         = errors.New("invalid person names")
	InvalidPersonCreationTime = errors.New("invalid person creation time")
)

func (p Person) Ok() error {
	if p.Sid == 0 && p.FirstName == "root" && p.LastName == "root" {
		// root is always valid.
		return nil
	}

	_idType := strings.TrimSpace(p.IdType)
	_id := strings.TrimSpace(p.Id)
	if !validId(_idType, _id) {
		return InvalidPersonId
	}

	_firstName := strings.TrimSpace(p.FirstName)
	_lastName := strings.TrimSpace(p.LastName)
	_moms := strings.TrimSpace(p.Moms)
	if !validNames(_firstName, _lastName, _moms) {
		return InvalidPersonName
	}

	_gender := strings.TrimSpace(p.Gender)
	if !validGender(_gender) {
		return InvalidPersonGender
	}

	if p.Created <= 0 && !p.isRoot() {
		return InvalidPersonCreationTime
	}
	return nil
}

func (p *Person) isRoot() bool {
	return p.Sid == 0
}

func validId(idType, id string) bool {
	return idType == "DNI" && len(id) > 4
}

func validNames(first, last, moms string) bool {
	firstValid := len(first) >= 2
	lastEmpty := len(last) == 0
	momsEmpty := len(moms) == 0
	lastValid := len(last) > 2
	momsValid := len(moms) > 2
	validLastNames := (lastValid && momsEmpty) || (lastEmpty && momsValid) || (lastValid && momsValid)
	return firstValid && validLastNames
}

func validGender(gen string) bool {
	return gen == "M" || gen == "F" || gen == "UN"
}
