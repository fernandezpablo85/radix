package model

type Role struct {
	Code           string `json:"code"`
	Label          string `json:"label"`
	OrganizationId int    `json:"organizationId"`
}
