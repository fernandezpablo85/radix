package model

import "errors"

type Credentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

var (
	EmptyEmail    = errors.New("Empty email")
	EmptyPassword = errors.New("Empty password")
)

func (c Credentials) Ok() error {
	if len(c.Email) <= 0 {
		return EmptyEmail
	}
	if len(c.Password) <= 0 {
		return EmptyPassword
	}
	return nil
}
