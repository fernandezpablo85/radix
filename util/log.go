package util

import (
	"log"
	"os"
)

var Logger = log.New(os.Stdout, "[radix]", log.Ldate|log.Ltime|log.Lshortfile)
