package util

import (
	"errors"
	"reflect"
	"testing"
)

var emptyFooError = errors.New("empty foo")

type sample struct {
	Foo string `json:"foo"`
	Bar string `json:"bar"`
}

func (s sample) Ok() error {
	if len(s.Foo) <= 0 {
		return emptyFooError
	} else {
		return nil
	}
}

func TestPointerDecode(t *testing.T) {
	s1 := sample{"", "bar"}

	// value
	err := tryValidate(s1)
	if err != emptyFooError {
		t.Fatalf("expected error %v but was %v", emptyFooError, err)
	}

	// pointer
	err = tryValidate(&s1)
	if err != emptyFooError {
		t.Fatalf("expected error %v but was %v", emptyFooError, err)
	}
}

func TestSampleDecode(t *testing.T) {
	var tests = []struct {
		s             sample
		expectedError error
	}{
		{sample{"foo", "bar"}, nil},
		{sample{"", "bar"}, emptyFooError},
	}

	for _, ts := range tests {
		reader, err := ToJsonReader(ts.s)
		if err != nil {
			t.Fatalf("error while encoding sample %v", err)
		}
		var result sample
		err = Decode(reader, &result)
		if err != ts.expectedError {
			t.Fatalf("expected error %v but was %v", ts.expectedError, err)
		}
		if !reflect.DeepEqual(result, ts.s) {
			t.Fatalf("expected %v to be equal to %v", result, ts.s)
		}
	}
}

func TestSampleDecodeSlice(t *testing.T) {
	s1 := sample{"foo", "bar"}
	s2 := sample{"", "bar"}
	var items []interface{} = []interface{}{s1, s2}

	err := tryValidate(items)
	if err != emptyFooError {
		t.Fatalf("expected error %v but was %v", emptyFooError, err)
	}
}
