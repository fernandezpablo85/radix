package util

import (
	"bytes"
	"encoding/json"
	"io"
	"reflect"
)

type ok interface {
	Ok() error
}

func tryValidateSlice(vals []interface{}) error {
	var err error = nil
	for i := range vals {
		err = tryValidateSingle(vals[i])
		if err != nil {
			break
		}
	}
	return err
}

func tryValidateSingle(val interface{}) error {
	if validable, ok := val.(ok); ok {
		Logger.Printf("validating type %T", val)
		err := validable.Ok()
		if err != nil {
			Logger.Printf("error validating %T: %v for %v", val, err, val)
		}
		return err
	} else {
		Logger.Printf("%T is not validable", val)
		return nil
	}
}

func tryValidate(val interface{}) error {
	rvalue := reflect.ValueOf(val)

	switch rvalue.Kind() {

	case reflect.Ptr:
		return tryValidate(reflect.Indirect(rvalue).Interface())

	case reflect.Slice:
		slice := make([]interface{}, rvalue.Len())
		for i := range slice {
			slice[i] = rvalue.Index(i).Interface()
		}
		return tryValidateSlice(slice)

	default:
		return tryValidateSingle(val)
	}
}

func Decode(reader io.Reader, val interface{}) error {
	err := json.NewDecoder(reader).Decode(val)
	if err != nil {
		Logger.Printf("error decoding %T, error: %s", val, err.Error())
		return err
	}

	return tryValidate(val)
}

func ToJsonReader(val interface{}) (io.Reader, error) {
	bytes_, err := json.Marshal(val)
	if err != nil {
		Logger.Printf("error encoding %v to json: %v", val, err)
		return nil, err
	} else {
		return bytes.NewReader(bytes_), nil
	}
}
