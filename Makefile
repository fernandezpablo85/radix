.PHONY: build test test-cover
all: build

build:
	@go build ./...

test:
	@go test ./...

install:
	@go install ./...

test-cover:
	@echo "mode: set" > acc.coverage-out
	@go test -coverprofile=client.coverage-out ./client
	@cat client.coverage-out | grep -v "mode: set" >> acc.coverage-out
	@go test -coverprofile=model.coverage-out ./model
	@cat model.coverage-out | grep -v "mode: set" >> acc.coverage-out
	@go test -coverprofile=util.coverage-out ./util
	@cat util.coverage-out | grep -v "mode: set" >> acc.coverage-out
	@go tool cover -html=acc.coverage-out
	@rm *.coverage-out
