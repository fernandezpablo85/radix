package client

import (
	"fmt"
	"strings"
)

func Path(host string, port int, path string, args ...interface{}) string {
	base := fmt.Sprintf("http://%s:%d", host, port)
	_path := fmt.Sprintf(path, args...)
	return base + _path
}

func Ids(ids []int) string {
	ret := ""
	for i := 0; i < len(ids); i++ {
		ret += fmt.Sprintf("%d,", ids[i])
	}
	ret = strings.TrimRight(ret, ",")
	return ret
}
