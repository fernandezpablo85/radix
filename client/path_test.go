package client

import "testing"

var tests = []struct {
	host     string
	port     int
	path     string
	args     []interface{}
	expected string
}{
	{"example.com", 80, "/foo/%s/baz", []interface{}{"bar"}, "http://example.com:80/foo/bar/baz"},
	{"example.com.ar", 8080, "/patient/%d/evolution/%d/action/%s", []interface{}{1, 100, "do"}, "http://example.com.ar:8080/patient/1/evolution/100/action/do"},
}

func TestPathInterpolation(t *testing.T) {
	for _, test := range tests {
		result := Path(test.host, test.port, test.path, test.args...)
		if test.expected != result {
			t.Fatalf("expected %s but got %s", test.expected, result)
		}
	}
}

func TestIds(t *testing.T) {
	ids := []int{1, 2, 3, 4, 5, 7, 124}
	expected := "1,2,3,4,5,7,124"
	got := Ids(ids)
	if got != expected {
		t.Fatalf("expected ids: %s but got %s", expected, got)
	}
}
