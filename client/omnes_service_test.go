package client

import (
	"bitbucket.org/fernandezpablo85/radix/model"
	"bitbucket.org/fernandezpablo85/radix/util"
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"
)

var (
	samplePerson = model.Person{Sid: 1, Id: "12341234", IdType: "DNI", FirstName: "Pablo", LastName: "Fernandez", Gender: "M", Created: 12341234}
)

type mockTransport struct {
	status int
	err    error
	body   interface{}
}

func (t mockTransport) jsonBody() io.ReadCloser {
	if t.body == nil {
		return ioutil.NopCloser(bytes.NewReader(nil))
	}
	reader, _ := util.ToJsonReader(t.body)
	return ioutil.NopCloser(reader)
}

func (t mockTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if t.err != nil {
		return nil, t.err
	} else {
		return &http.Response{
			Header:     http.Header{},
			Request:    req,
			StatusCode: t.status,
			Body:       t.jsonBody(),
		}, nil
	}
}

func httpClient(status int, body interface{}, err error) http.Client {
	client := http.Client{}
	client.Transport = mockTransport{status, err, body}
	return client
}

func newClient(status int, body interface{}, err error) *HttpOmnesClient {
	return &HttpOmnesClient{httpClient(status, body, err), "localhost", 8080}
}

func TestPingOk(t *testing.T) {
	client := newClient(200, nil, nil)
	err := client.Ping()
	if err != nil {
		t.Fatalf("Ping should return nil error for 200 response")
	}
}

func TestPingFail(t *testing.T) {
	client := newClient(500, nil, nil)
	err := client.Ping()
	if err != OmnesServiceError {
		t.Fatalf("Ping should return %v for 500 response, but got %v", OmnesServiceError, err)
	}
}

func TestSavePerson(t *testing.T) {
	var response = struct {
		Sid int `json:"sid"`
	}{1234}

	client := newClient(200, response, nil)
	person := model.Person{}
	id, err := client.SavePerson(&person)
	if err != nil {
		t.Fatalf("error while decoding id: %v", err)
	}
	if id != response.Sid {
		t.Fatalf("should return %d but got %d", response.Sid, id)
	}
	if person.Sid != response.Sid {
		t.Fatalf("person object should have Sid = %d but instead has %d", response.Sid, person.Sid)
	}
}

func TestFindPersonByIdNotFound(t *testing.T) {
	client := newClient(404, nil, nil)
	_, err := client.FindPersonById(1)
	if err != OmnesPersonNotFound {
		t.Fatalf("expected person not found error but got %v", err)
	}
}

func TestFindPersonByIdError(t *testing.T) {
	client := newClient(500, nil, nil)
	_, err := client.FindPersonById(1)
	if err != OmnesServiceError {
		t.Fatalf("expected omnes service error but got %v", err)
	}
}

func TestFindPersonById(t *testing.T) {
	client := newClient(200, samplePerson, nil)
	result, err := client.FindPersonById(1)
	if err != nil {
		t.Fatalf("unexpected error %v", err)
	}

	if !reflect.DeepEqual(result, &samplePerson) {
		t.Fatalf("expected %v to be equal to %v", result, samplePerson)
	}
}

func TestGetPersons(t *testing.T) {
	people := []model.Person{samplePerson}
	client := newClient(200, people, nil)
	result, err := client.GetPersons([]int{1, 2, 3})
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if !reflect.DeepEqual(result, people) {
		t.Fatalf("expected %v to be equal to %v", result, samplePerson)
	}
}

func TestFindMatches(t *testing.T) {
	distances := []model.PersonDistance{model.PersonDistance{Score: 100.0, Candidate: samplePerson}}
	client := newClient(200, distances, nil)
	result, err := client.FindMatches(&samplePerson)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if !reflect.DeepEqual(result, distances) {
		t.Fatalf("expected %v to be equal to %v", result, distances)
	}
}
