package client

import (
	"bitbucket.org/fernandezpablo85/radix/model"
	"bitbucket.org/fernandezpablo85/radix/util"
	"errors"
	"net/http"
	"strings"
	"time"
)

type Pinger interface {
	Ping() error
	PingOrDie()
}

type OmnesClienter interface {
	FindPersonById(int) (*model.Person, error)
	FindMatches(*model.Person) ([]model.PersonDistance, error)
	GetPersons([]int) ([]model.Person, error)
	SavePerson(*model.Person) (int, error)
	FindPeopleByName(string) ([]model.Person, error)
	FindPeopleByPersonalId(int) ([]model.Person, error)
	Pinger
}

type HttpOmnesClient struct {
	httpClient http.Client
	host       string
	port       int
}

var (
	NameTooShortError                 = errors.New("Name too short, minimum length is 3")
	OmnesPersonNotFound               = errors.New("Person not found")
	OmnesServiceError                 = errors.New("Error reaching Omnes service")
	DefaultOmnesClient  OmnesClienter = &HttpOmnesClient{
		httpClient: http.Client{Timeout: 4 * time.Second},
		host:       "localhost",
		port:       8081,
	}
)

func isOk(code int) bool {
	return code >= 200 && code < 400
}

func (o *HttpOmnesClient) Path(path string, args ...interface{}) string {
	return Path(o.host, o.port, path, args...)
}

func (o *HttpOmnesClient) FindPersonById(id int) (*model.Person, error) {
	path := o.Path("/api/v1/Person/%d", id)
	response, err := o.httpClient.Get(path)
	if err != nil {
		util.Logger.Printf("error while reaching omnes service: %v", err)
		return nil, err
	}

	switch {
	case response.StatusCode == 404:
		return nil, OmnesPersonNotFound
	case !isOk(response.StatusCode):
		util.Logger.Printf("Omnes request failed, status = %d", response.StatusCode)
		return nil, OmnesServiceError
	}

	var person model.Person
	err = util.Decode(response.Body, &person)
	if err != nil {
		util.Logger.Printf("error while reading person: %v", err)
		return nil, err
	}
	return &person, nil
}

func (o *HttpOmnesClient) FindMatches(person *model.Person) ([]model.PersonDistance, error) {
	path := o.Path("/api/v1/Person/_validate")
	reader, err := util.ToJsonReader(person)
	if err != nil {
		return nil, err
	}
	response, err := o.httpClient.Post(path, "application/json", reader)
	if err != nil {
		util.Logger.Printf("error while reaching matches service: %v", err)
		return nil, err
	}

	var matches []model.PersonDistance
	err = util.Decode(response.Body, &matches)
	if err != nil {
		util.Logger.Printf("error reading person distances, %v", err)
		return nil, err
	}
	return matches, nil
}

func (o *HttpOmnesClient) GetPersons(pids []int) ([]model.Person, error) {
	path := o.Path("/api/v1/Person?ids=%s", Ids(pids))
	response, err := o.httpClient.Get(path)
	if err != nil {
		util.Logger.Printf("error while reaching omnes service: %v", err)
		return nil, err
	}

	var people []model.Person
	err = util.Decode(response.Body, &people)
	if err != nil {
		util.Logger.Printf("error while reading people: %v", err)
		return nil, err
	}

	if err != nil {
		return nil, err
	} else {
		return people, nil
	}
}

func (o *HttpOmnesClient) SavePerson(person *model.Person) (int, error) {
	path := o.Path("/api/v1/Person")
	reader, err := util.ToJsonReader(person)
	if err != nil {
		return 0, err
	}
	response, err := o.httpClient.Post(path, "application/json", reader)
	if err != nil {
		util.Logger.Printf("error while reaching omnes: %v", err)
		return 0, err
	}
	if !isOk(response.StatusCode) {
		util.Logger.Printf("request to omnes failed with status: %s", response.Status)
		return 0, OmnesServiceError
	}
	var ret struct {
		Sid int `json:"sid"`
	}
	err = util.Decode(response.Body, &ret)
	if err != nil {
		util.Logger.Printf("error while reading response body: %v", err)
		return 0, err
	}
	person.Sid = ret.Sid
	return person.Sid, nil
}

func (o *HttpOmnesClient) Ping() error {
	path := o.Path("/healthcheck")
	response, err := o.httpClient.Get(path)
	if err != nil {
		return err
	}
	if !isOk(response.StatusCode) {
		return OmnesServiceError
	}
	return nil
}

func (o *HttpOmnesClient) PingOrDie() {
	err := o.Ping()
	if err != nil {
		util.Logger.Fatalf("can't reach Omnes at %s:%d, error: %v", o.host, o.port, err)
	}
}

func (o *HttpOmnesClient) FindPeopleByName(name string) ([]model.Person, error) {
	trimmed := strings.Trim(name, " ")
	if len(trimmed) < 3 {
		return nil, NameTooShortError
	} // TODO(vgiordano): analizar y efectuar si esta porción de código que valida el parametro debe ser movida a omnes

	path := o.Path("/api/v1/people/find?name=%s", trimmed)
	response, err := o.httpClient.Get(path)
	if err != nil {
		util.Logger.Printf("error while reaching omnes service: %v", err)
		return nil, err
	}

	if !isOk(response.StatusCode) {
		util.Logger.Printf("Omnes request failed, status = %d", response.StatusCode)
		return nil, OmnesServiceError
	}

	var people []model.Person
	err = util.Decode(response.Body, &people)
	if err != nil {
		util.Logger.Printf("error while reading people: %v", err)
		return nil, err
	}
	return people, nil
}

func (o *HttpOmnesClient) FindPeopleByPersonalId(personalId int) ([]model.Person, error) {
	path := o.Path("/api/v1/people/find/ByPersonalId/%d", personalId)
	response, err := o.httpClient.Get(path)
	if err != nil {
		util.Logger.Printf("error while reaching omnes service: %v", err)
		return nil, err
	}

	if !isOk(response.StatusCode) {
		util.Logger.Printf("Omnes request failed, status = %d", response.StatusCode)
		return nil, OmnesServiceError
	}

	var people []model.Person
	err = util.Decode(response.Body, &people)
	if err != nil {
		util.Logger.Printf("error while reading people: %v", err)
		return nil, err
	}
	return people, nil
}
